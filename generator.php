<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Random - Wilder - Generator</title>
    <link rel="stylesheet" href="style.css">
    <script src="main.js" defer></script>
</head>
<body>
<a href="index.php"><= Home</a>
<a href="bookedWilders.php">Booked - Wilders =></a>

<h1>Random Wilder Generator</h1>


<form action="bookedWilders.php" method="post" class="randomWilder">

    <div class="wilders">
        <label for="wilders">Enter Wilders : </label>
        <textarea name="wilders" id="wilders" cols="30" rows="10"></textarea>
    </div>

    <div class="date">
        <label for="dayOne">Choose Day One :</label>
        <input type="date" id="dayOne" name="dayOne">
    </div>

    <div class="date">
        <label for="lastDay">Choose Last Day :</label>
        <input type="date" id="lastDay" name="lastDay">
    </div>

    <div>
        <label for="chackAll">Check All</label>
        <input type="checkbox" id="chackAll" class="selectedDays" name="chackAll">
    </div>

    <div>
        <label for="Monday">Monday</label>
        <input type="checkbox" id="Monday" class="selectedDays" name="Monday">
    </div>
    <div>
        <label for="Tuesday">Tuesday</label>
        <input type="checkbox" id="Tuesday" class="selectedDays" name="Tuesday">
    </div>
    <div>
        <label for="Wednesday">Wednesday</label>
        <input type="checkbox" id="Wednesday" class="selectedDays" name="Wednesday">
    </div>
    <div>
        <label for="Thursday">Thursday</label>
        <input type="checkbox" id="Thursday" class="selectedDays" name="Thursday">
    </div>
    <div>
        <label for="Friday">Friday</label>
        <input type="checkbox" id="Friday" class="selectedDays" name="Friday">
    </div>
    <div>
        <label for="Saturday">Saturday</label>
        <input type="checkbox" id="Saturday" class="selectedDays" name="Saturday">
    </div>
    <div>
        <label for="Sunday">Sunday</label>
        <input type="checkbox" id="Sunday" class="selectedDays" name="Sunday">
    </div>


    <div class="submit">
        <input type="submit" value="Submit!" id="submit">
    </div>

</form>

</body>
</html>