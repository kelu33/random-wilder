const submit = document.getElementById('submit');

const selectedDays = document.querySelectorAll('.selectedDays');

selectedDays[0].addEventListener('change', function (event) {
    let check = false;
    if (event.currentTarget.checked) {
        check = true;
    }
    for (let day of selectedDays) {
        day.checked = check;
        day.addEventListener('change', function (event) {
            if (!event.currentTarget.checked) {
                check = true;
                selectedDays[0].checked = false;
            }
        })
    }
})

submit.addEventListener('click', function (event) {
    event.preventDefault();

    let wilders = document.getElementById('wilders').value.split(',');
    let dayOne = document.getElementById('dayOne').value;
    let lastDay = document.getElementById('lastDay').value;

    let mappedDays = dayMapping(selectedDays);

    wilders = arrayShuffle(wilders);
    let bookedWilders = bookPeeps(wilders, dayOne, lastDay, mappedDays);

    let listHtml = document.createElement('ul');
    listHtml.classList.add('wildList');
    let listContent = '';
    for (let i = 0; i < bookedWilders.length; i++) {
        listContent += `<li>${bookedWilders[i].name} ${bookedWilders[i].date}</li>`;
    }
    listHtml.innerHTML = listContent;

    document.body.appendChild(listHtml);

    let save = document.createElement('button');
    save.textContent = '^ Save ^';
    document.body.appendChild(save);

    save.onclick = function () {
        console.log(listContent);
        document.cookie = "list=" + listContent;
        // window.location.href = "https://localhost/randomWilder/bookedWilders.php";
    }
});

/**
 *
 * @param days
 * @returns {*[]}
 */
function dayMapping(days) {
    let dayMapping = [];
    if (days[0].checked) dayMapping = [0,1,2,3,4,5,6];
    else {
        for (let i = 0; i < days.length; i++) {
            if (days[i].checked) {
                dayMapping.push(i);
            } else {
                dayMapping.push(null);
            }
        }
        dayMapping.shift();
        let sunday = dayMapping.pop();
        if (sunday) sunday = 0;
        dayMapping.unshift(sunday);
    }
    return dayMapping;
}

/**
 *
 * @param peeps
 * @param date
 * @param end
 * @param days
 * @returns {*[]}
 */
function bookPeeps(peeps,date,end,days) {
    let endDate = new Date(end);
    let bookedPeeps = [];
    for (let i=0 ; i < peeps.length ; i++ ){
        bookedPeeps.push(
            {
                name: peeps[i],
                date: new Date(date)
            }
        );
    }

    let start = bookedPeeps[0].date.getDay();

    let count = 0;
    while (
        (days[0] < start || days[0] == null) && // TODO! debug condition
        count < 7
        ) {
        days.push(days.shift());
        count++;
    }
    let diff;
    days[0] >= start ? diff = days[0] - start : diff = 7 - start + days[0];

    /*let gap = [];
    for (let i = 0; i < days.length; i++) {
        if (days[i] == null) gap.push(true);
        else gap.push(false);
    }*/

    console.log('------------------------');
    console.log(days);
    // console.log('------------------------');
    // console.log(gap);

    for (let i = 0 ; i < bookedPeeps.length; i++){
        bookedPeeps[i].date.setDate(bookedPeeps[i].date.getDate() + diff);

        bookedPeeps[i].date = bookedPeeps[i].date.toDateString();
    }
    return bookedPeeps;
}

/**
 *
 * @param array
 * @returns {*[]}
 */
function arrayShuffle(array) {
    let shuffle = [];
    for (let i=0 ; i< array.length ; i++ ){
        shuffle.splice(
            getRandomInteger(shuffle.length),
            0,
            array[i]);
    }
    return shuffle;
}

/**
 * Return a number >= 0 et < max
 * @param max
 * @returns {number}
 */
function getRandomInteger(max){
    return Math.floor(Math.random()*max);
}